const http = require('http');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const CONFIG = require('./config.json');

process.env.CONFIG = JSON.stringify(
    Object.assign(
        {},
        CONFIG,
        Object.keys(CONFIG)
            .filter(k => /(rootDir|contentDirectory|presentationDirectory)/.test(k))
            .reduce(
                (obj, k) => Object.assign({}, obj, { [k]: path.join(__dirname, CONFIG[k]) }),
                {},
            ),
    ),
);

const IOController = require('./app/controllers/io.controller.js');
const { defaultRoute, presentationRoute } = require('./app/routes');
const middleware = require('./app/middleware/authMiddleware');

const app = express();
const server = http.createServer(app);

app.use(bodyParser.json());

app.use(defaultRoute);
// Applying the middleware seems to be based on the order of the creation of the routes
app.use(middleware.adminMiddleware);

app.use(presentationRoute);
app.use(
    '/admin',
    express.static(path.join(__dirname, 'public/admin')),
);
app.use(
    '/web',
    express.static(path.join(__dirname, 'public/watch')),
);

app.use((req, res, next) => {
    const err = Object.assign({}, new Error('Not Found'), { status: 404 });
    next(err);
});

app.use((err, req, res) => {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {},
    });
});

server.listen(CONFIG.port, () => {
    // eslint-disable-next-line no-console
    console.log('server listens on ', CONFIG.port, '...');
    IOController.listen(server);
});
