// source : https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens
const jwt = require('jsonwebtoken');

function filterByGroup(req, res, next, role) {
    // check header or url parameters or post parameters for token
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'secret', (err, decoded) => {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            }
            // if everything is good, save to request for use in other routes
            if (decoded.role === role) {
                req.decoded = decoded;
                return next();
            }
            return res.status(403).send();
        });
    }
    // if there is no token
    // return an error
    return res.status(403).send({
        success: false,
        message: 'No token provided.',
    });
}

module.exports = {
    adminMiddleware: (req, res, next) => {
        filterByGroup(req, res, next, 'ADMIN');
    },
    watcherMiddleware: (req, res, next) => {
        filterByGroup(req, res, next, 'WATCHER');
    },
};
