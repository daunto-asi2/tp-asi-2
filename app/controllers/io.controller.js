/* eslint-disable no-param-reassign,no-console */

const socketio = require('socket.io');
const presentationController = require('../controllers/presentationController');
const ContentModel = require('../models/content.model');

const IoController = {

    listen(httpServer) {
        const io = socketio(httpServer);

        const socketList = {};

        io.on('connection', (socket) => {
            console.log('Connection established');
            socket.slideId = 0;
            socket.presId = -1;
            socket.emit('connection');

            socket.on('data_comm', (message) => {
                socketList[message.id] = socket;
            });

            socket.on('slidEvent', (message) => {
                if (message.CMD === 'START' && message.PRES_ID) {
                    socket.presId = message.PRES_ID;
                }
                presentationController.loadPresById(socket.presId)
                    .then((pres) => {
                        switch (message.CMD) {
                        case 'NEXT':
                            socket.slideId += 1;
                            break;
                        case 'PREV':
                            socket.slideId = socket.slideId > 0
                                ? socket.slideId - 1
                                : 0;
                            break;
                        default:
                            break;
                        }
                        const { contentMap } = pres.slidArray[socket.slideId];

                        return Promise.all([pres].concat(Object.keys(contentMap).map(
                            elt => ContentModel.read(contentMap[elt], (err, contentModel) =>
                                new Promise((resolve, reject) => (
                                    err
                                        ? reject(err)
                                        : resolve(contentModel)
                                )),
                            ),
                        )));
                    })
                    .then(([pres, ...contentModels]) => {
                        pres.slidArray[socket.slideId].contentList = contentModels;
                        socket.emit('slidEvent_pong', pres.slidArray[socket.slideId]);
                    })
                    .catch(err => console.error(err));
            });
        });
    },
};

module.exports = IoController;
