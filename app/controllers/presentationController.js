const path = require('path');
const {
    readFileIfExists,
    glob,
    writeFile,
    getPresFilePath,
} = require('../utils');

const CONFIG = JSON.parse(process.env.CONFIG);

const PresentationController = {
    loadPres() {
        return glob(CONFIG.presentationDirectory, file => /\.json$/.test(file))
            .then(fileList =>
                Promise.all(
                    fileList.map(
                        file => readFileIfExists(path.join(CONFIG.presentationDirectory, file)),
                    ),
                ),
            )
            .then(filesContents =>
                filesContents.reduce((obj, currentContent) => {
                    const content = JSON.parse(currentContent);
                    return Object.assign(obj, { [`pres.${content.id}`]: content });
                }, {}),
            );
    },
    loadPresById(id) {
        return this.loadPres()
            .then(pres => pres.filter(p => p.id === id))
            .then(presWithId => (
                presWithId.length > 0
                    ? presWithId[0]
                    : Promise.reject(new Error('Not found'))
            ));
    },
    savePres(pres) {
        if (!pres.id) {
            return Promise.reject(new Error('ID must be defined and unique'));
        }
        return writeFile(getPresFilePath(`${pres.id}.pres.json`), JSON.stringify(pres));
    },
};

module.exports = PresentationController;
