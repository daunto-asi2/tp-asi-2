/** env jest */

const promisify = require('../promisify');

function asyncWithValue(id, cb) {
    return cb(null, id);
}

function asyncWithErr(id, cb) {
    return cb(new Error('error'));
}

test('promisify when success', (done) => {
    promisify(asyncWithValue)(34)
        .then((id) => {
            expect(id).toBe(34);
            done();
        });
});

test('promisify when failure', (done) => {
    promisify(asyncWithErr)(34)
        .catch((err) => {
            expect(err).toBeInstanceOf(Error);
            done();
        });
});
