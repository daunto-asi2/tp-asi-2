/** env jest */

const nodify = require('../nodify');

test('nodify when success', () => {
    nodify(id => Promise.resolve({ id }))(34, (err, user) => {
        expect(err).toBeNull();
        expect(user).toHaveProperty('id');
        expect(user.id).toBe(34);
    });
});

test('nodify when failure', () => {
    nodify(() => Promise.reject(new Error('error')))((err) => {
        expect(err).toBeInstanceOf(Error);
    });
});
