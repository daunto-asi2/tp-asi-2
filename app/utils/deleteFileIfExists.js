const fs = require('fs');

const fileExists = require('./fileExists');

/**
 * @param {String} filePath the path of the file to delete
 * @return {Promise<Buffer>}
 */
module.exports = function deleteFileIfExists(filePath) {
    return fileExists(filePath)
        .then(() =>
            new Promise((resolve, reject) => {
                fs.unlink(filePath, (err) => {
                    if (err) return reject(err);

                    return resolve();
                });
            }),
        () => Promise.resolve());
};
