/**
 * This method returns a promisified version of a standard async function
 * provided as parameter
 *
 * @param {Function} asyncFunctionThatTakesACallback a async function
 * with a standard node error handling callback
 * @return {Function}
 */
module.exports = function promisify(asyncFunctionThatTakesACallback) {
    return (...args) =>
        asyncFunctionThatTakesACallback(...args, (err, ...rest) =>
            new Promise((resolve, reject) => (
                err
                    ? reject(err)
                    : resolve(...rest)
            )),
        );
};
