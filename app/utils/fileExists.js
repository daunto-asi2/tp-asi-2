const fs = require('fs');

module.exports = function fileExists(filePath) {
    return new Promise((resolve, reject) => {
        fs.stat(filePath, (err, stat) => {
            if (err) {
                return reject(err);
            }

            if (!stat.isFile()) {
                return reject(new Error('file is not file'));
            }

            return resolve();
        });
    });
};
