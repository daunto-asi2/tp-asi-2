const path = require('path');

const CONFIG = JSON.parse(process.env.CONFIG);

/**
 * @param {String} fileName the filename path
 * @return {String} the contentDirectory path + filename path
 */
module.exports = function getDataFilePath(fileName) {
    return path.join(CONFIG.contentDirectory, fileName);
};
