const path = require('path');

const CONFIG = JSON.parse(process.env.CONFIG);

module.exports = function getPresFilePath(filename) {
    return path.join(CONFIG.presentationDirectory, filename);
};
