const path = require('path');

const CONFIG = JSON.parse(process.env.CONFIG);

module.exports = function getMetaFilePath(id) {
    return path.join(CONFIG.contentDirectory, `${id}.meta.json`);
};
