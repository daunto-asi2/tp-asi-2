const fs = require('fs');

const fileExists = require('./fileExists');

/**
 * @param {String} filePath the path of the file to read
 * @return {Promise<Buffer>}
 */
module.exports = function readFileIfExists(filePath) {
    return fileExists(filePath)
        .then(() =>
            new Promise((resolve, reject) => {
                fs.readFile(filePath, (err, buffer) => {
                    if (err) return reject(err);

                    return resolve(buffer.toString());
                });
            }),
        );
};
