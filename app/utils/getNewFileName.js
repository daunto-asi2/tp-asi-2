module.exports = function getNewFileName(id, originalFileName) {
    return `${id}.${originalFileName.split('.').pop()}`;
};
