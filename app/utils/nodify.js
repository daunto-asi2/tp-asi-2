/**
 * This function nodify a promise,
 * it returns a function which takes
 * all the params of the previous
 * promised function but that takes
 * a standard node error handling
 * callback as last argument
 *
 * @param {Function} functionThatReturnsPromise a function that returns
 * a Promise
 * @return {Function} a function with a changed signature
 */
module.exports = function nodify(functionThatReturnsPromise) {
    return (...args) => {
        const callback = args.pop();
        functionThatReturnsPromise(...args)
            .then((v) => { callback(null, v); })
            .catch((e) => { callback(e); });
    };
};
