const fs = require('fs');

/**
 * @param {String} rootPath the root path to search the file from
 * @param {Function} filterCallback the filter callback to use to
 * filter the list of found files in `rootPath `
 * @return {Promise<Object>}
 */
module.exports = function glob(rootPath, filterCallback) {
    return new Promise((resolve, reject) => {
        fs.readdir(rootPath, (err, data) => {
            if (err) {
                return reject(err);
            }
            return resolve(data.filter(filterCallback));
        });
    });
};
