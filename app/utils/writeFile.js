const fs = require('fs');
/**
 * @param {String} filepath the path of the file to write on disk
 * @param {String} content the content to write to the file
 * @return {Promise<void>} The promise resolves if the write is successful
 */
module.exports = function writeFile(filepath, content) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filepath, content, (err) => {
            if (err) return reject(err);

            return resolve();
        });
    });
};
