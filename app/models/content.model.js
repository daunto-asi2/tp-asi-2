/* eslint-disable class-methods-use-this */
const {
    deleteFileIfExists,
    fileExists,
    getDataFilePath,
    getMetaFilePath,
    promisify,
    readFileIfExists,
    writeFile,
} = require('../utils');

class ContentModel {
    constructor({
        type,
        id,
        title,
        src,
        fileName,
    } = {}) {
        this.type = type;
        this.id = id;
        this.title = title;
        this.src = src;
        this.fileName = fileName;

        let data;

        this.getData = () => data;

        this.setData = (newData) => {
            data = newData;
        };
    }

    static normalizeOutput(key, value) {
        if (key === 'data') return undefined;
        return value;
    }

    static create(content, callback) {
        if (!(content instanceof ContentModel)) {
            return callback(new Error('content should be a ContentModel'));
        }

        if (!content.id) {
            return callback(new Error('id is mandatory'));
        }

        const filename = getDataFilePath(content.fileName);
        const metafile = getMetaFilePath(content.id);
        return (() => ((content.getData() !== undefined)
            ? writeFile(filename, content.getData())
            : Promise.resolve()
        ))()
            .then(writeFile(metafile, JSON.stringify(content, ContentModel.normalizeOutput)))
            .then(() => callback(null))
            .catch(err => callback(err));
    }

    static thenCreate(content) {
        return promisify(ContentModel.create)(content);
    }

    static read(id, callback) {
        if (!id) {
            return callback(new Error('id is mandatory'));
        }

        return readFileIfExists(getMetaFilePath(id))
            .then(content => new ContentModel(JSON.parse(content)))
            .then(contentModel => Promise.all([
                contentModel,
                readFileIfExists(getDataFilePath(contentModel.fileName)),
            ]))
            .then(([contentModel, data]) => {
                contentModel.setData(data);
                callback(null, contentModel);
            })
            .catch(err => callback(err));
    }

    static thenRead(id) {
        return promisify(ContentModel.read)(id);
    }

    static delete(id, callback) {
        return ContentModel.read(
            id,
            (err, contentModel) => {
                if (err || !contentModel) {
                    callback(err || new Error('ContentModel is undefined'));
                } else {
                    deleteFileIfExists(getMetaFilePath(contentModel.id))
                        .then(() => deleteFileIfExists(getDataFilePath(contentModel.fileName)))
                        .then(() => callback(null))
                        .catch(error => callback(error));
                }
            },
        );
    }

    static thenDelete(id) {
        return promisify(ContentModel.delete)(id);
    }

    /**
     * @param {ContentModel} content
     * @param {Function} callback
     */
    static update(content, callback) {
        const metaFile = getMetaFilePath(content.id);
        return fileExists(metaFile)
            .then(
                () => ContentModel.create(content, callback),
                () => Promise.reject(new Error(`${metaFile} does not exist`)),
            )
            .catch(err => callback(err));
    }

    /**
     * @param {ContentModel} content
     */
    static thenUpdate(content) {
        return promisify(ContentModel.update)(content);
    }
}

module.exports = ContentModel;
