const express = require('express');

const PresentationController = require('../controllers/presentationController');

const router = express.Router();

router.route('/loadPres')
    .get((request, response) => {
        PresentationController.loadPres()
            .then(content => response.json(content))
            .catch((err) => {
                console.error(err);
                response.status(500).send(err);
            });
    });

router.route('/savePres')
    .post((request, response) => {
        PresentationController.savePres(request.body)
            .then(content => response.json(content))
            .catch(err => response.status(500).send(err));
    });

module.exports = router;
