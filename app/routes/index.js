const defaultRoute = require('./default.route');
const presentationRoute = require('./presentation.route');

module.exports = {
    defaultRoute,
    presentationRoute,
};
