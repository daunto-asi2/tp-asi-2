const express = require('express');
const jwt = require('jsonwebtoken');
const axios = require('axios');

const router = express.Router();
const CONFIG = JSON.parse(process.env.CONFIG);

router.route('/')
    .get((req, res) => {
        res.send('it works!');
    });

router.route('/auth')
    .options((req, res) => {
        // CORS For AJAX requests!
        res.set({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
        }).status(204).send();
    })
    .post((req, res, next) => {
        res.set({
            'Access-Control-Allow-Origin': '*',
        });
        return next();
    }, (req, res) => {
        axios.post(CONFIG.authServer, {
            login: req.body.login,
            pwd: req.body.pwd,
        }).then((response) => {
            if (response.data.validAuth) {
                const token = jwt.sign(response.data, 'secret', {
                    expiresIn: 1440,
                });
                res.json({ token });
            } else {
                res.status(401).json({
                    status: 'Unauthorized',
                    status_code: 401,
                    error_code: 4011,
                    message: 'Unable to authenticate',
                });
            }
        }).catch((error) => {
            console.error(error);
            res.status(500).json({
                status: 'Server Error',
                status_code: 500,
                error_code: 5000,
                message: 'An error occured',
            });
        });
    });

module.exports = router;
