/* eslint-disable no-console */

const fs = require('fs');

const CONFIG = require('./config.json');

process.env.CONFIG = JSON.stringify(CONFIG);

const {
    generateUUID,
    readFileIfExists,
    getMetaFilePath,
    getDataFilePath,
} = require('./app/utils');
const ContentModel = require('./app/models/content.model.js');

const content = new ContentModel();

content.id = generateUUID();
content.type = 'myType';
content.title = 'myTitle';
content.fileName = `${content.id}.txt`;
content.setData('It Works !');

console.log('---------- ContentModel ----------');
console.dir(ContentModel);
console.log('-------------------------------');
console.log('------------ content -------------');
console.dir(content);
console.log('-------------------------------');

function countFile() {
    const files = fs.readdirSync(CONFIG.contentDirectory);
    console.log(`Count files : ${files.length} files in ${CONFIG.contentDirectory}`);
    return files.length;
}

function testCreate(contentModel) {
    console.log('====== TEST CREATE =======');
    console.dir(contentModel);

    return new Promise((resolve, reject) => {
        const nbFiles = countFile();
        ContentModel.create(contentModel, (err) => {
            if (err) {
                console.error(err);
                return reject(err);
            }

            if (countFile() - nbFiles !== 2) {
                return reject(new Error('Les fichiers n\'ont pas été créés correctement'));
            }

            return readFileIfExists(getMetaFilePath(contentModel.id))
                .then((data) => {
                    if (JSON.parse(data.toString()).data) {
                        return reject(new Error('Le champ \'data\' ne doit pas apparaitre dans le fichier de meta-données'));
                    }

                    return readFileIfExists(getDataFilePath(contentModel.fileName));
                })
                .then((data) => {
                    if (data.toString() !== contentModel.getData()) {
                        return reject(new Error('Les fichiers n\'ont pas été créés correctement'));
                    }
                    return resolve(contentModel);
                })
                .catch(reject);
        });
    });
}
/**
 * @param {ContentModel} content
 */
function testRead(contentModel) {
    console.log('====== TEST READ =======');
    console.dir(contentModel);

    return new Promise((resolve, reject) => {
        ContentModel.read(contentModel.id, (err, data) => {
            if (err) {
                console.error(err);
                return reject(err);
            }
            console.log(data);

            return resolve(data);
        });
    });
}

function testUpdate(paramContentModel) {
    console.log('====== TEST UPDATE =======');
    console.dir(paramContentModel);
    const contentModel = paramContentModel;
    contentModel.title = 'MOD_title';
    const newData = `${contentModel.getData()} YES,  IT IS !!!`;
    contentModel.setData(newData);

    return new Promise((resolve, reject) => {
        ContentModel.update(contentModel, (err) => {
            if (err) {
                console.error(err);
                return reject(err);
            }
            console.dir(contentModel);

            return readFileIfExists(getDataFilePath(contentModel.fileName))
                .then((data) => {
                    if (data.toString() !== newData) {
                        return reject(new Error('Les données ont mal été mises à jour'));
                    }
                    return resolve(contentModel);
                });
        });
    });
}

function testDelete(contentModel) {
    console.log('====== TEST DELETE =======');
    console.dir(contentModel);

    return new Promise((resolve, reject) => {
        const nbFiles = countFile();
        ContentModel.delete(contentModel.id, (err) => {
            if (err) {
                console.error(err);
                return reject(err);
            }

            if (nbFiles - countFile() !== 2) {
                return reject(new Error('Probleme lors de la suppression des fichiers'));
            }

            console.dir('Slid supprimee');
            return resolve();
        });
    });
}

function logError(err) {
    console.error('>>> ERROR');
    console.error(err);
    console.error('<<< ERROR');
}

function testErr(initialContent) {
    console.log('====== TEST ERROR =======');
    const contentTest = new ContentModel(12);
    console.dir(contentTest);
    const contentCopy = initialContent;

    return testCreate(12)
        .then(console.log, (err) => {
            logError(err);

            contentCopy.id = null;
            return contentCopy;
        })
        .then(testCreate)
        .then(console.log, (err) => {
            logError(err);
            return contentCopy;
        })
        .then(testRead)
        .then(console.log, (err) => {
            logError(err);
            return contentCopy;
        })
        .then(testUpdate)
        .then(console.log, (err) => {
            logError(err);
            contentCopy.id = 12;
            return contentCopy;
        })
        .then(testUpdate)
        .then(console.log, (err) => {
            logError(err);
            return contentCopy;
        });
}

(function main() {
    testCreate(content)
        .then(testRead)
        .then(testUpdate)
        .then(testDelete)
        .then(() => {
            console.log('========== TESTS PHASE 1 : OK ==========');
            return content;
        }, (err) => {
            logError(err);
            return Promise.reject(new Error('========== TESTS PHASE 1 : KO =========='));
        })
        .then(testErr)
        .then(() => {
            console.log('========== TESTS PHASE 2 : OK ==========');
        }, err => Promise.reject(err || new Error('========== TESTS PHASE 2 : KO ==========')))
        .then(() => {
            console.log('========== FIN TESTS ==========');
        }, (err) => {
            console.log(err.message);
        });
}());
